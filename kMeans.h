#ifndef KMEANS_H_INCLUDED
#define KMEANS_H_INCLUDED

#include <iostream>
#include <fstream>
#include <unordered_map>
#include <string>
#include <random>

#include "GraphVertex.h"
#include "GraphEdge.h"
#include "cluster.h"
#include "Graph.h"

using namespace std;

class kMeans{

   vector<cluster*> agrupa(Graph grafo);
   float calculaDist(int id1, int id2, Graph grafo);
   void insereNoCluster(GraphVertex* no, int id , float dist);
   void recalculaCentro();

};
#endif // KMEANS_H_INCLUDED
