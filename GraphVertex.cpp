/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "GraphVertex.h"

GraphVertex::GraphVertex(int idVertex)
{
    this->id = idVertex;
}

GraphVertex::GraphVertex(int idVertex, double vertexWeight)
{
    this->id = idVertex;
    this->vertexWeight = vertexWeight;
    this->dist =  -1000;
}


int GraphVertex::getId()
{
    return this->id;
}

double GraphVertex::getDist(){
return this->dist;
}

void GraphVertex::setDist(double dist){
this->dist = dist;
}
void GraphVertex::addEdge(GraphEdge* edge)
{
    edgeList.insert(pair<int, GraphEdge*>(edge->getId(), edge));

}

GraphEdge* GraphVertex::getEdge(int idEdge)
{
    edgeList.at(idEdge);
}

unordered_map<int, GraphEdge*> GraphVertex::getEdgeList()
{
    return edgeList;
}

void GraphVertex::removeEdge(int idEdge)
{
    edgeList.erase(idEdge);
}

int GraphVertex::getDegree()
{
    return static_cast<int>(edgeList.size());
}

double GraphVertex::getVertexWeight()
{
    return this->vertexWeight;
}
