/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   GraphVertex.h
 * Author: jna
 *
 * Created on 20 de Outubro de 2018, 20:33
 */

#ifndef GRAPHVERTEX_H
#define GRAPHVERTEX_H

#include <unordered_map>
#include "GraphEdge.h"

using namespace std;

/*
 * Classe que representa um vertice do grafo
 */
class GraphVertex
{
private:
    int id;//Id do vertice
    double vertexWeight;//Peso do vertice
    double dist;// distancia at� seu cluster atual
    unordered_map<int, GraphEdge*> edgeList;//Arestas incidentes no vertice
public:
    GraphVertex(int idVertex);//Construtor
    GraphVertex(int idVertex, double vertexWeight);
    int getId();//Retorna o id do vertice
    void addEdge(GraphEdge *edge);//Adiciona aresta ao vertice
    GraphEdge* getEdge(int idEdge);//Retorna aresta a partir do id
    unordered_map<int, GraphEdge*> getEdgeList();//Retorna a lista de arestas do vertice
    void removeEdge(int idEdge);//Remove a aresta do vertice
    int getDegree();//Retorna o grau do vertice
    double getVertexWeight();
    double getDist();// retorna dist
    void setDist(double dist);

};





#endif /* GRAPHVERTEX_H */

