
#ifndef KM_H_INCLUDED
#define KM_H_INCLUDED

#include <iostream>
#include <fstream>
#include <unordered_map>
#include <string>
#include <random>

#include "GraphVertex.h"
#include "GraphEdge.h"
#include "cluster.h"
#include "Graph.h"
#include <vector>

using namespace std;

class Km{

private:
    Graph *grafo;
    int k;

public:
    Km(Graph *grafo, int k);
    vector<cluster*> agrupa();

};
#endif // KMEANS_H_INCLUDED
