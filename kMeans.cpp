#include <cstdlib>
#include <iostream>
#include <fstream>
#include <chrono>

#include "Graph.h"
#include "kMeans.h"
using namespace std;

float kMeans::calculaDist(int id1, int id2, Graph grafo){
    for (int i=0 ; i< grafo.getEdgeList().size() ; i++ ){
        if(grafo.getEdge(i)->getIdFirstVertex() == id1  && grafo.getEdge(i)->getIdSecondVertex() == id2 )
            return grafo.getEdge(i)->getWeight();
        }
    }




vector<cluster*>kMeans::agrupa(Graph grafo){
vector<cluster*> clusters ;

// determinar k e c
int k=0 ,  c=0;
int beneficios;
int atualiza =1;
float dist;
float beneficioCluster[k];
float limiteMaximo,limiteMin;

// escolha aleatoria dos k centros
if ((c*k) >= beneficios){
    for(int i =0 ;  i<k ; i++){
        cluster* clusterAux = new cluster();
        clusterAux->lista.push_back(grafo.getVertex(i));
        clusters.push_back(clusterAux);
        clusters.at(i)->setIdCentro(grafo.getVertex(i)->getId());
        clusters.at(i)->setBeneficio(clusters.at(i)->getBeneficio() + grafo.getVertex(i)->getVertexWeight() );
    }
}
//inicializa as distancias
for (int i =0 ; i< grafo.getVertexList().size() ; i++){
    grafo.getVertex(i)->setDist(9999999999999999);
}

while(atualiza == 1){
        //associa ao mais proximo
        for(int i = 0 ; i < grafo.getVertexList().size() ; i++){
            for (int j=0 ; j< k  ; j++){
                dist = calculaDist(i,clusters.at(j)->getIdCentro(),grafo);
                //insere no cluster
                if(grafo.getVertex(i)->getDist() > dist ){
                        clusters.at(k)->lista.push_back(grafo.getVertex(i));
                        clusters.at(k)->setBeneficio(clusters.at(k)->getBeneficio() + grafo.getVertex(i)->getVertexWeight());
                }
            }
        }

    //---------------------------
        int idNovoCluster;
        float distNovoCluster;
        for (int i = 0 ; i < k ; i++){
            while(clusters.at(k)->getBeneficio() > limiteMaximo || clusters.at(k)->getBeneficio() < limiteMin ){
                // j= todos os vertices do cluster
                for(int j = 0 ; j < clusters.at(i)->lista.size() ; j++){
                    // todos os clusters
                    for (int n = 0 ; n < k ; n++){
                        if(n != k){
                            if( clusters.at(n)->getBeneficio() + clusters.at(i)->lista.at(j)->getVertexWeight() < limiteMaximo){
                                float dist1 = calculaDist(clusters.at(i)->lista.at(j)->getId() , clusters.at(n)->getIdCentro() , grafo);
                                if(dist1<clusters.at(i)->lista.at(j)->getDist()){
                                    idNovoCluster = n;
                                    distNovoCluster = dist1;
                                }
                            }
                        }
                         //insere no novo cluster
                                clusters.at(idNovoCluster)->lista.push_back(clusters.at(i)->lista.at(j));
                         // atualiza distancia
                                clusters.at(idNovoCluster)->lista.back()->setDist(distNovoCluster);
                         // remove do cluster anterior
                                clusters.at(i)->lista.erase(clusters.at(i)->lista.begin(),clusters.at(i)->lista.begin()+j);
                    }
                }
            }
        }

        //---------------------------------------------------------------------------- recalcula centro

        for (int i =0 ; i < k ; i++){
         clusters.at(i)->setTamanhoCluster(clusters.at(i)->lista.size());
         for(int j = 1 ; j< clusters.at(i)->lista.size() ; j++ ){
            clusters.at(i)->mediaAux = (calculaDist(clusters.at(i)->lista.at(0)->getId(),clusters.at(i)->lista.at(j)->getId(),grafo) / clusters.at(i)->getTamanhoCluster());
         }


        }
        float mediaAtual = 0;
        int contadorEstabilidade = 0 ;
        for (int i=0 ; i< k ; i++){
                int mediaInicial = clusters.at(i)->mediaAux;
            for (int j=0 ; j < clusters.at(i)->lista.size() ; j++ ){
                for(int n=0 ; n < clusters.at(i)->lista.size() ; n++ ){
                    if ( n != j){
                        mediaAtual += calculaDist(clusters.at(i)->lista.at(n)->getId(),clusters.at(i)->lista.at(j)->getId(),grafo) / clusters.at(i)->getTamanhoCluster();
                    }
                }
                if(mediaAtual < clusters.at(i)->mediaAux){
                    clusters.at(i)->setIdCentro(clusters.at(i)->lista.at(j)->getId());
                    clusters.at(i)->mediaAux =  mediaAtual;
                }
            }
            if (mediaInicial - clusters.at(i)->mediaAux == 0 ){
                contadorEstabilidade++;
            }
            if(contadorEstabilidade == k){
                atualiza = 0;
            }

        }
        //--------------------------------------------------------------------------------------

}

return clusters;
}

