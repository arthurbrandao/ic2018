/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Graph.h
 * Author: jna
 *
 * Created on 20 de Outubro de 2018, 20:57
 */

#ifndef GRAPH_H
#define GRAPH_H

#include <iostream>
#include <fstream>
#include <unordered_map>
#include <string>
#include <random>

#include "GraphVertex.h"
#include "GraphEdge.h"

using namespace std;

/*
 *Classe que representa um grafo
 */
class Graph {
private:
    bool oriented; //Indica se o grafo eh orientado ou nao
    unordered_map<int, GraphVertex*> vertexList; //Guarda os vertices do grafo
    unordered_map<int, GraphEdge*> edgeList; //Guarda as arestas do grafo
public:
    Graph(bool oriented);
    Graph(string filename);
    Graph(Graph *graph);
    ~Graph();

    void load(string filename); //Carrega um grafo a partir de um arquivo
    void save(string filename);

    void print(); //imprime os vertices e arestas do grafo no console
    bool isOriented(); //Verifica se o grafo eh orientado
    void addVertex(GraphVertex *vertex); //Adiciona um vertice ao grafo
    GraphVertex* getVertex(int idVertex); //Retorna o vertice a partir do id
    unordered_map<int, GraphVertex*> getVertexList(); //Retorna a lista de vertices do grafo
    void removeVertex(int idVertex); //Remove um vertice do grafo
    int vertexCount(); //Retorna a quantidade de vertices do grafo
    void addEdge(GraphEdge *edge); //Adiciona uma aresta ao grafo
    GraphEdge* getEdge(int idEdge); //Retorna uma aresta a partir do id
    unordered_map<int, GraphEdge*> getEdgeList(); //Retorna a lista de arestas do grafo
    void removeEdge(int idEdge); //Remove uma aresta do grafo
    int edgeCount(); //Retorna a quantidade de arestas do grafo


};
#endif /* GRAPH_H */

