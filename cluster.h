#ifndef CLUSTER_H_INCLUDED
#define CLUSTER_H_INCLUDED

#include <iostream>
#include <fstream>
#include <unordered_map>
#include <string>
#include <random>

#include "GraphVertex.h"
#include "GraphEdge.h"

using namespace std;

class cluster{

public:
    int idCentro;
    float beneficio;
    float mediaAux;
    vector<GraphVertex*> lista;
    int getIdCentro();
    void setIdCentro(int id);
    float getBeneficio();
    void setBeneficio(float beneficio);
    int tamanhoCluster;
    int getTamanhoCluster();
    void setTamanhoCluster(int tamanho);


};
#endif //
