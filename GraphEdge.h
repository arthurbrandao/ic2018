/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   GraphEdge.h
 * Author: jna
 *
 * Created on 20 de Outubro de 2018, 20:44
 */

#ifndef GRAPHEDGE_H
#define GRAPHEDGE_H

/*
 *Classe que representa uma aresta do grafo
 */
class GraphEdge
{
private:
    int id;
    int idFirstVertex;
    int idSecondVertex;
    double weight;
public:
    GraphEdge(int idEdge, int idFirstVertex, int idSecondVertex, double weight);
    int getId();
    int getIdFirstVertex();
    int getIdSecondVertex();
    double getWeight();
    
};


#endif /* GRAPHEDGE_H */

