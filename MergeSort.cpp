/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include <vector>

#include "MergeSort.h"

void MergeSort::merge(vector<GraphVertex*>* vertexList, int start, int end, int middle)
{
    vector<GraphVertex*> tmpVertexList(end - start +1);
    int i = start;
    int j = middle + 1;
    int k = 0;
    
    while((i <= middle) && (j <= end))
    {
        if(vertexList->at(i)->getDegree() > vertexList->at(j)->getDegree())
        {
            tmpVertexList[k++] = vertexList->at(i);
            i++;
        }
        else
        {
            tmpVertexList[k++] = vertexList->at(j);
            j++;
        }
    }
    
    while(i <= middle)
    {
        tmpVertexList[k++] = vertexList->at(i);
        i++;
    }
    
     while(j <= end)
    {
        tmpVertexList[k++] = vertexList->at(j);
        j++;
    }
    
    for(i = start; i <= end; i++)
    {
        vertexList->at(i) = tmpVertexList[i-start];
    }
}

void MergeSort::sort(vector<GraphVertex*> *vertexList, int start, int end)
{
    if(start < end)
    {
        int middle = (start + end)/2;
        sort(vertexList, start, middle);
        sort(vertexList, middle + 1, end);
        merge(vertexList, start, end, middle);        
    }
}