/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "Graph.h"

Graph::Graph(bool oriented)
{
    this->oriented = false;
}
Graph::Graph(string filename)
{
    ifstream graphFile;
    
    graphFile.open(filename);

    //if(graphFile.is_open())
    //{
        int vertexCount;// Variavel que guarda a quantidade de vertices do grafo
        graphFile >> vertexCount;//Faz a leitura da quantidade de vertices do grafo
        int clusterCount;
        graphFile >> clusterCount;
        cout<<vertexCount<<endl;
        string separator;
        graphFile >>separator;
        
        int lowerBound;
        int upperBound;
        
        for(int i = 0; i < clusterCount; i++)
        {
            graphFile >> lowerBound;
            graphFile >> upperBound;            
        }
        
                
        for(int i = 1; i < vertexCount; i++)//Adiciona a quantidade de vertices ao grafo
        {
            addVertex(new GraphVertex(i));
        }
        
        int idEdge = 0;//id da aresta
        int idFirstVertex, idSecondVertex;//id dos vertices da aresta
        double adjWeight = 0;//peso da aresta
        
        while(!graphFile.eof())
        {
            graphFile >> idFirstVertex >> idSecondVertex >> adjWeight;//Faz a leitura da aresta do grafo
            addEdge(new GraphEdge(idEdge, idFirstVertex, idSecondVertex, adjWeight));//Adiciona a aresta ao grafo
            idEdge++;
        }
        graphFile.close();
    //}
}
Graph::Graph(Graph* graph)
{
    this->oriented = graph->isOriented();
    
    for(auto it = graph->vertexList.begin();it != graph->vertexList.end(); it++)
    {
        addVertex(new GraphVertex(it->second->getId()));
    }
    
    for(auto it = graph->vertexList.begin(); it != graph->vertexList.end(); it++)
    {
        unordered_map<int, GraphEdge*> tmpAdjList = it->second->getEdgeList();
        
        for(auto it2 = tmpAdjList.begin(); it2 != tmpAdjList.end(); it2++)
        {
            addEdge(new GraphEdge(it2->second->getId(), it2->second->getIdFirstVertex(),
                    it2->second->getIdSecondVertex(), it2->second->getWeight()));
        }
    }
}

Graph::~Graph()
{
    for(auto it = vertexList.begin(); it != vertexList.end(); it++)
    {
        delete it->second;
    }
    
    for(auto it = edgeList.begin(); it != edgeList.end(); it++)
    {
        delete it->second;
    }    
}

void Graph::load(string filename)
{
    ifstream graphFile;
    graphFile.open(filename);
    
    if(graphFile.is_open())
    {
        int vertexCount;// Variavel que guarda a quantidade de vertices do grafo
        graphFile >> vertexCount;//Faz a leitura da quantidade de vertices do grafo
        cout<<vertexCount<<endl;
        for(int i = 1; i <= vertexCount; i++)//Adiciona a quantidade de vertices ao grafo
        {
            addVertex(new GraphVertex(i));
            
        }
        
        int idEdge = 1;//id da aresta
        int idFirstVertex, idSecondVertex;//id dos vertices da aresta
        double adjWeight = 0;//peso da aresta
        
        while(!graphFile.eof())
        {
            graphFile >> idFirstVertex >> idSecondVertex >> adjWeight;//Faz a leitura da aresta do grafo
            addEdge(new GraphEdge(idEdge, idFirstVertex, idSecondVertex, adjWeight));//Adiciona a aresta ao grafo
            idEdge++;
        }
        graphFile.close();
    }
}

void Graph::save(string filename)
{
    ofstream graphFile(filename.c_str(), ofstream::out | ofstream::binary);
    graphFile << vertexCount() << std::endl;
    
    for(auto& edge : edgeList)
    {
        graphFile << edge.second->getIdFirstVertex() << " "
                  << edge.second->getIdSecondVertex() << " "
                  <<std::endl;
    }
    
    graphFile.close();
}

bool Graph::isOriented()
{
    return this->oriented;
}

void Graph::addVertex(GraphVertex* vertex)
{
    this->vertexList.insert(pair<int, GraphVertex*>(vertex->getId(),vertex));//Adiciona o vertice à lista de vertices do grafo
}

GraphVertex* Graph::getVertex(int idVertex)
{
    return vertexList.at(idVertex);//Retorna o vertice com "idVertex" da lista de vertices do grafo
}

unordered_map<int, GraphVertex*> Graph::getVertexList()
{
    return vertexList;//Retorna a lista de vertices do grafo
}

void Graph::removeVertex(int idVertex)
{
    GraphVertex *vertex = getVertex(idVertex);//Obtem o vertice do grafo a partir do "idVertex"
    unordered_map<int, GraphEdge*> vertexEdgeList = vertex->getEdgeList();//Obtem a lista de arestas do vertice
    
    for(auto it = vertexEdgeList.begin(); it != vertexEdgeList.end(); it++)//Remove todas as arestas do vertice do grafo
    {
        removeEdge(it->first);//Remove a aresta do grafo
    }
    
    vertexList.erase(vertex->getId());//Remove o vertice
    delete vertex;            
}

int Graph::vertexCount()
{
    return static_cast<int>(vertexList.size());
}

void Graph::addEdge(GraphEdge* edge)
{
    edgeList.insert(pair<int, GraphEdge*>(edge->getId(), edge));//Adiciona a aresta à lista de arestas do grafo
    GraphVertex *firstVertex = getVertex(edge->getIdFirstVertex());//Obtem o primeiro vertice da aresta a partir do Id do vertice
    firstVertex->addEdge(edge);//Adiciona a aresta na lista de arestas do vertice
    
    if(!isOriented())
    {
        GraphEdge *otherEdge = new GraphEdge((-1 * edge->getId()),
                                              edge->getIdSecondVertex(),
                                              edge->getIdFirstVertex(),
                                              edge->getWeight());//Cria aresta oposta a que esta sendo adicionada
        edgeList.insert(pair<int, GraphEdge*>(otherEdge->getId(), otherEdge));//Insere a aresta oposta na lista de arestas do grafo
        GraphVertex *secondVertex = getVertex((otherEdge->getIdFirstVertex()));//Obtem o vertice do grafo a partir do id do vertice
        secondVertex->addEdge(otherEdge);//Adiciona a aresta na lista de arestas do vertice
    }
}

GraphEdge* Graph::getEdge(int idEdge)
{
    return edgeList.at(idEdge);//Retorna a aresta com "idEdge" da lista de vertices
}

unordered_map<int, GraphEdge*> Graph::getEdgeList()
{
    return edgeList;//Retorna a lista de arestas
}

void Graph::removeEdge(int idEdge)
{
    GraphEdge *edge = getEdge(idEdge);//Obtem a aresta do grafo a partir do ID
    GraphVertex *firstVertex = getVertex(edge->getIdFirstVertex());//Obtem o primeiro vertice da aresta a partir do id do vertice
    firstVertex->removeEdge(edge->getId());//Remove a aresta da lista de arestas do vertice
    edgeList.erase(idEdge);//Remove a aresta da lista de arestas do grafo
    
    if(!isOriented())
    {
        GraphEdge *otherEdge = getEdge((-1 * edge->getId()));//Obtem a aresta oposta à que esta sendo removida
        GraphVertex *secondVertex = getVertex(otherEdge->getIdFirstVertex());//Obtem o vertice do grafo a partir do id
        secondVertex->removeEdge(otherEdge->getId());//Remove a aresta da lista de arestas do vertice
        edgeList.erase(otherEdge->getId());//Remove a aresta da lista de arestas do grafo
        delete otherEdge;
    }
    delete edge;
}

int Graph::edgeCount()
{
    return static_cast<int>(edgeList.size());//Retorna a quantidade de arestas do grafo
}