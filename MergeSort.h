/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MergeSort.h
 * Author: jna
 *
 * Created on 20 de Outubro de 2018, 21:22
 */

#ifndef MERGESORT_H
#define MERGESORT_H

#include "GraphVertex.h"

/*
 *Classe que implementa o algoritmo de ordenação MergeSort
 */
class MergeSort
{
private:
    static void merge(vector<GraphVertex*> *vertexList, int start, int end, int middle);
public:
    static void sort(vector<GraphVertex*> *vertexList, int start, int end);
};

#endif /* MERGESORT_H */

