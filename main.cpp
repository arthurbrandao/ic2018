/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   main.cpp
 * Author: jna
 *
 * Created on 20 de Outubro de 2018, 20:27
 */
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <chrono>
#include <iterator>

#include "Graph.h"
#include "kMeans.h"
#include "cluster.h"
#include "Km.h"
#include <vector>

using namespace std;

int main(int argc, char* argv[]) {

    Graph *graph = new Graph(false);
    vector<cluster*> grupos;

    ifstream graphFile;

    int vertexCount; // Variavel que guarda a quantidade de vertices do grafo
    int clusterCount;
    string separator;
    string W;
    int lowerBound;
    int upperBound;
    double clusterCapacity;
    int idEdge = 0; //id da aresta
    int idFirstVertex, idSecondVertex; //id dos vertices da aresta
    double adjWeight = 0; //peso da aresta
    vector<double> vertexWeight;
    string line;
    graphFile.open("C:/Users/Arthur/Desktop/Nova pasta (2)/ic2018/instancias-Grupos/TipoD/20_5_A.txt", ios::in);

    double temp;

    switch (2) {
        case 1:
        {
            if (graphFile.is_open()) {

                graphFile >> vertexCount; //Faz a leitura da quantidade de vertices do grafo
                graphFile >> clusterCount;
                graphFile >>separator;

                for (int i = 0; i < clusterCount; i++) {
                    graphFile >> lowerBound;
                    graphFile >> upperBound;
                }

                graphFile >> W;

                for (int i = 0; i < vertexCount; i++) {
                    graphFile >> temp;
                    vertexWeight.push_back(temp);
                }

                for (int i = 0; i < vertexCount; i++)//Adiciona a quantidade de vertices ao grafo
                {
                    graph->addVertex(new GraphVertex(i, vertexWeight.at(i)));
                }

                while (std::getline(graphFile, line)) {
                    //Faz a leitura da aresta do grafo
                    graphFile >> idFirstVertex >> idSecondVertex >> adjWeight;
                    //cout<<idFirstVertex<<" "<<idSecondVertex<<" "<<adjWeight<<endl;
                    graph->addEdge(new GraphEdge(idEdge, int(idFirstVertex), int(idSecondVertex), double(adjWeight))); //Adiciona a aresta ao grafo
                    idEdge++;
                }
            }
        }
        case 2:
        {
            if (graphFile.is_open()) {

                graphFile >> vertexCount; //Faz a leitura da quantidade de vertices do grafo
                graphFile >> clusterCount;
                graphFile >> clusterCapacity;
                for (int i = 0; i < vertexCount; i++) {
                    graphFile >> temp;
                    vertexWeight.push_back(temp);
                }

                for (int i = 0; i < vertexCount; i++)//Adiciona a quantidade de vertices ao grafo
                {
                    graph->addVertex(new GraphVertex(i, vertexWeight.at(i)));
                }
                idEdge = 0;
                double distanceMatrix[vertexCount][vertexCount];
                while (std::getline(graphFile, line)) {
                    //Faz a leitura da aresta do grafo
                    for (int i = 0; i < vertexCount; i++)
                        for (int j = 0; j < vertexCount; j++) {
                            graphFile >> adjWeight;
                            distanceMatrix[i][j] = adjWeight;
                            graph->addEdge(new GraphEdge(idEdge,i ,j, double(adjWeight))); //Adiciona a aresta ao grafo
                            idEdge++;
                        }


                }

            }
        }

    }
    cout << graph->vertexCount() << endl;
    cout<<graph->edgeCount()<<endl;

    Km *km = new Km(graph,clusterCount);
    vector<cluster*> v = km->agrupa();
    for(int i =0 ; i< v.size() ; i++){
        cout<< "cluster "<<i<< " = :"<<endl;
        for( int j = 0 ; j < v.at(i)->lista.size() ; j++ ){
            cout << "elemento : "<< v.at(i)->lista.at(j)->getId()<<endl ;
        }
    }

        double media = 0 ;
        for(int i =0 ; i< v.size() ; i++){
        cout<< "beneficio do cluster "<<i<< " = "<<v.at(i)->getBeneficio() <<endl;
        media += v.at(i)->getBeneficio();
    }
        media = media/5;

        cout << "m�dia = "<< media;


    cout << graph->edgeCount() << endl;
    graphFile.close();
    delete graph;
    return 0;
}
