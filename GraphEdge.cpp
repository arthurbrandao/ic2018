/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "GraphEdge.h"

GraphEdge::GraphEdge(int idEdge, int idFirstVertex, int idSecondVertex, double weight)
{
    this->id = idEdge;
    this->idFirstVertex = idFirstVertex;
    this->idSecondVertex = idSecondVertex;
    this->weight = weight;
}

int GraphEdge::getId()
{
    return this->id;
}

int GraphEdge::getIdFirstVertex()
{
    return this->idFirstVertex;    
}

int GraphEdge::getIdSecondVertex()
{
    return this->idSecondVertex;
}

double GraphEdge::getWeight()
{
    return this->weight;
}